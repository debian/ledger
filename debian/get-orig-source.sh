#!/bin/bash

UPSTREAM_VERSION=$1
UPSTREAM_HASH=$2

git archive --prefix=ledger-${UPSTREAM_VERSION}/ ${UPSTREAM_HASH} | gzip -9 > ../ledger_${UPSTREAM_VERSION}.orig.tar.gz 

